from flask import Flask, url_for, redirect ,request

app = Flask(__name__)

VERIFY_TOKEN = "Peru"
#se crea una variable para el token
@app.route('/')
def index():
    #
    if(request.args.get('hub.verify_token','') == VERIFY_TOKEN):
        return request.args.get('hub.challenge')
    
    else:
        print("Token fallido")
        return 'Error'


if (__name__) == '__main__':
    app.run(debug=True, port=8086)